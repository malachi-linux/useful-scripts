#!/bin/bash
#
# fuse of E2 sets 8mhz clock, otherwise it's 8 / 1
PIO=~/.platformio
if [ -z $AVRDUDE_PORT ]; then
    AVRDUDE_PORT=$1
fi
#AVRDUDE_DIR=$PIO/packages/tool-avrdude
#AVRDUDE=$AVRDUDE_DIR/avrdude
AVRDUDE=avrdude
#CONF="-C $AVRDUDE_DIR/avrdude.conf"
PROTO=buspirate
#PROTO=2232HIO
MPU=attiny85

$AVRDUDE $CONF -c $PROTO -P $AVRDUDE_PORT -p $MPU -v -U flash:w:.pioenvs/attiny/firmware.hex -U lfuse:w:0xe2:m
