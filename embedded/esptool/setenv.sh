#!/bin/bash

. ../esp-open-rtos/setenv.sh

export ESPTOOL_PORT=$ESPPORT

# yaota8266 needs this, otherwise it falls back to incompatible AR on macOS
export AR=xtensa-lx106-elf-ar
