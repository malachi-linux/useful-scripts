#!/bin/bash

if [ -z "$MBED_TARGET" ]; then
    export MBED_TARGET=$(mbed target | sed 's/\[mbed\] //')
    echo "MBED_TARGET not set, setting to $MBED_TARGET"
fi
export MBED_TOOLCHAIN=$(mbed toolchain | sed 's/\[mbed\] //')

PROJECT_NAME=${PWD##*/}

STM_MEM_ADDR=0x8000000

st-flash write BUILD/$MBED_TARGET/$MBED_TOOLCHAIN/$PROJECT_NAME.bin $STM_MEM_ADDR
