#!/bin/bash

# TODO: Make a profile override via an environment variable, or
# if this feature already exists in mbed-cli leverage that

if [ -z "$MBED_COMPILE_PROFILE" ]
then
    MBED_COMPILE_PROFILE=debug
    echo "No default profile specified.  Using '$MBED_COMPILE_PROFILE'"
fi

mbed compile --profile profiles/$MBED_COMPILE_PROFILE.json $1 $2 $3 $4
