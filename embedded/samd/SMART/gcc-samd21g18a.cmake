# has toolchain specific settings for gcc + START + a specific CPU arch - i.e. samd21g

include(${CMAKE_CURRENT_LIST_DIR}/gcc-arm.cmake)

set(ARCH_DIR ${START_DIR}/samd21a/gcc)
if(NOT DEFINED LINKER_SCRIPT)
    message("Warning: linker script should be set before atmel-start makefile is reached")
    set(LINKER_SCRIPT ${ARCH_DIR}/gcc/samd21g18a_flash.ld)
endif(NOT DEFINED LINKER_SCRIPT)

# clumsy - in CLion it's hard to load in toolchain file, so we specify toolchain seperately
# but this means CPU_OPTIONS don't get set, so fallback here.  definitely hacky.  Echoes the
# semi-hacky nature of specifying the ARCH_DIR also.  To fix *that*, we need a further-up
# toolchain-y file which starts with the atmel processor arch and cascades down from there into
# particular ARM and GCC toolchain variants.  Being that ARCH_DIR is GCC dependent, we should
# specify GCC (or otherwise) earlier rather than later
if(NOT DEFINED CPU_OPTIONS)
# NOTE: arch.cmake IS CPU specific, so consider explicitly setting CPU_OPTIONS here instead of doing
# an include
    include(${CMAKE_CURRENT_LIST_DIR}/../../atmel/gcc-samd21g18a.cmake)
endif(NOT DEFINED CPU_OPTIONS)

# add in actual meat of start project lib
# (picks up CMakeLists.txt)
add_subdirectory(${START_CMAKE_DIR})