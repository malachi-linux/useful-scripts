# semi-atmel-start-specific settings
# it is assumed all atmel start projects work with either SAM ARM or AVR
# designed to be included from gcc-ARCH.cmake specific file

# where all the cmake/helpers live (should be CMAKE_CURRENT_LIST_DIR directory, ideally)
if(NOT DEFINED START_CMAKE_DIR)
    set(START_CMAKE_DIR ${CMAKE_CURRENT_LIST_DIR})
endif(NOT DEFINED START_CMAKE_DIR)

# probably we can share these across most ARM implementations
add_compile_options(
    $<$<COMPILE_LANGUAGE:C>:-std=c11>
    $<$<COMPILE_LANGUAGE:CXX>:-fms-extensions>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-exceptions>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-rtti>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-use-cxa-atexit>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-threadsafe-statics>
    -Os
    -mlong-calls
    -g3
    -Wall
    -pedantic
    -Wextra
    -fstrict-volatile-bitfields
    -ffunction-sections
)

# this is not as arch specific (just ARM targets) so probably shouldn't go into arch.cmake
# but is convenient to have here for now
include(${CMAKE_CURRENT_LIST_DIR}/../../arm/bin.cmake)

# sam specific flash tool
# shouldn't really go in this file but better than it living in the samd21g18a specific file
include(${CMAKE_CURRENT_LIST_DIR}/../../atmel/bossac.cmake)

