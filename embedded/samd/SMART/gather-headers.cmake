set(FREERTOS_DIR ${START_DIR}/thirdparty/RTOS/freertos/FreeRTOSV10.0.0)
set(FREERTOS_DIR2 ${FREERTOS_DIR}/Source)
set(FREERTOS_GCC_DIR ${FREERTOS_DIR}/Source/portable/GCC/ARM_CM0)

set(CMSIS_DIR ${START_DIR}/CMSIS/Include)
set(USB_DIR ${START_DIR}/usb)

set(HAL_DIR ${START_DIR}/hal)
set(HAL_UTILS_DIR ${HAL_DIR}/utils)

# glob this one recursively
set(HPL_DIR ${START_DIR}/hpl)

set(HRI_DIR ${START_DIR}/hri)

include_directories(${FREERTOS_DIR})
include_directories(${FREERTOS_DIR2}/include)
# just for hal_rtos.h
include_directories(${FREERTOS_DIR}/../..)
include_directories(${FREERTOS_GCC_DIR})

include_directories(${HAL_DIR}/include)
include_directories(${HAL_UTILS_DIR}/include)

include_directories(${HPL_DIR}/core
    ${HPL_DIR}/port
    ${HPL_DIR}/gclk
    ${HPL_DIR}/adc
    ${HPL_DIR}/dac
    ${HPL_DIR}/dmac
    ${HPL_DIR}/pm
    ${HPL_DIR}/rtc
    )

include_directories(${START_DIR}/config)
include_directories(${CMSIS_DIR})

include_directories(${ARCH_DIR}/../include)

include_directories(${HRI_DIR})

include_directories(${USB_DIR})
include_directories(${USB_DIR}/device)
include_directories(${USB_DIR}/class/cdc)
include_directories(${USB_DIR}/class/cdc/device)