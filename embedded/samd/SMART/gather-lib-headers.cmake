# to compile start, we need a very large set of include paths set up
# for the app itself, less so.  So put the app-only portion in gather-headers

include(${CMAKE_CURRENT_LIST_DIR}/gather-headers.cmake)

include_directories(${START_DIR})
