# auto-deducing entry point.  alternatively, one can specific the specific gcc-ARCH.cmake file directly
message(STATUS "Looking for start project conforming to arch ${ATMEL_ARCH_MODEL}")
if(ATMEL_ARCH STREQUAL SAMD)
    if(ATMEL_ARCH_MODEL STREQUAL SAMD21G18A)
        include(${CMAKE_CURRENT_LIST_DIR}/gcc-samd21g18a.cmake)
    endif()
endif()