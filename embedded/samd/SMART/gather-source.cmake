include(${CMAKE_CURRENT_LIST_DIR}/gather-lib-headers.cmake)

file(GLOB FREERTOS_SOURCE ${FREERTOS_DIR}/*.c 
    ${FREERTOS_DIR2}/*.c 
    ${FREERTOS_GCC_DIR}/*.c
    ${FREERTOS_DIR2}/portable/MemMang/*.c)

file(GLOB HAL_SOURCE ${HAL_DIR}/src/*.c ${HAL_UTILS_DIR}/src/*.c)

file(GLOB_RECURSE ARCH_SOURCE ${ARCH_DIR}/*.c)
file(GLOB_RECURSE HPL_SOURCE ${HPL_DIR}/*.c)
file(GLOB_RECURSE USB_SOURCE ${USB_DIR}/*.c)

file(GLOB MAIN_SOURCE ${START_DIR}/*.c)

# include everything but their boilerplate starter main.c, we'll provide our own
# (meaning we also must explicitly call atmel_start_init())
# since it expands to absolute paths, we need to regex
#list(REMOVE_ITEM MAIN_SOURCE "${START_DIR}/main.c")
list(FILTER MAIN_SOURCE EXCLUDE REGEX ".main.c$")