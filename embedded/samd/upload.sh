#!/bin/bash

# If BOSSAC_COMMAND not setup, extract from platformio
if [ -z "${BOSSAC_COMMAND}"]; then
    PIO=~/.platformio/packages/tool-bossac
    BOSSAC_COMMAND=$PIO/bossac
fi

if [ -z $2 ]; then
    TEST=$2
else
    FIRMWARE_PATH=$2
fi

DEVICE=$1

if [ -z "${DEVICE}" ]; then
  DEVICE=$BOSSAC_UPLOAD_PORT
fi

if [[ $DEVICE == /dev/* ]]; then
  echo "Error: drop the prepending '/dev/' from your device parameter"
fi

if [ -z "${DEVICE}" ]; then
    echo "No device path specified"
    exit -1
fi

if [ -z ${FULL_FIRMWARE_PATH} ]; then
# good reference http://serverfault.com/questions/7503/how-to-determine-if-a-bash-variable-is-empty
#if 2nd parameter is set to a non-empty string
    if [ -z "${FIRMWARE_PATH}" ]; then
    #  echo "Using TEST path: $2"
    #else
      echo "No FIRMWARE_PATH specified"
      exit
    fi
    FULL_FIRMWARE_PATH=.pioenvs/$FIRMWARE_PATH/firmware.bin
fi

echo "Using firmware path: $FULL_FIRMWARE_PATH"

$BOSSAC_COMMAND --info --port=$DEVICE --erase --write --verify --reset --debug $FULL_FIRMWARE_PATH
#$BOSSAC_COMMAND --info --port=$DEVICE
