set(ATMEL_BOARD_NAME "Adafruit Feather M0")

include(${CMAKE_CURRENT_LIST_DIR}/samd21-common.cmake)

add_definitions(-DATMEL_BOARD_FILE="feather-m0.h")
add_definitions(-DATMEL_BOARD_FEATHER_M0)

set(BOARD_LINKER_OPTIONS 
    -T${LINKER_SCRIPT}
    -Wl,--defsym=BOOTLOADER_SIZE=8k
    --specs=nano.specs
    -Wl,--gc-sections
    # reset handler needed to stoke linker into bringing in atmel-start lib
    -uReset_Handler
    )