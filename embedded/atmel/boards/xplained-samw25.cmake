set(ATMEL_BOARD_NAME "Xplained SAMW25")

include(${CMAKE_CURRENT_LIST_DIR}/samd21-common.cmake)

add_definitions(-DATMEL_BOARD_FILE="xplained-samw25.h")
add_definitions(-DATMEL_BOARD_XPLAINED_SAMW25)

set(BOARD_LINKER_OPTIONS 
    -T${LINKER_SCRIPT}
    -Wl,--defsym=BOOTLOADER_SIZE=0k
    --specs=nano.specs
    -Wl,--gc-sections
    -uReset_Handler
    )