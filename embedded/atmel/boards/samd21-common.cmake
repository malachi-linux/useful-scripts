message(STATUS "Targeting board: ${ATMEL_BOARD_NAME}")

# overriding what was specified in the gcc-samd21g18a.cmake
# eventually that one will interplay with this instead of
# merely being overriden
set(LINKER_SCRIPT ${CMAKE_CURRENT_LIST_DIR}/samd21g18a.ld)

# FIX: Phase out 'start' dependency here
#include(${CMAKE_CURRENT_LIST_DIR}/../start/gcc-samd21g18a.cmake)
