# this goes in arch because flashing tool is arch specific
# NOTE: Pretty soon this can be named properly to flash2 and we can have a atmel-flash.cmake file 
# or similar
add_custom_target(flash
    bossac -wev -R ${PROJECT_NAME}.bin
    DEPENDS ${PROJECT_NAME}.bin
    )
