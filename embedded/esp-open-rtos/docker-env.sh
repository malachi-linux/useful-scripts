#!/bin/bash

if [ -z $1 ]; then
export DOCKER_IMG=malachib/esp-open-rtos
else
export DOCKER_IMG=$1
fi

if [ "$(uname)" != "Darwin" ]; then
# linux and friends can do usb mapping like this
# macOS needs manual intervention for the particular USB device with docker-machine
  export DOCKER_USB_MAPPING_OPTION='-v /dev/bus/usb:/dev/bus/usb'
fi

# the VirtualBox connection does the job, (using /dev/bus/usb)
# so we don't need this
#if [ -z $ESPPORT ]; then
#    echo TEST
#else
#    export DOCKER_ESPPORT="--device=$ESPPORT -e "ESPPORT=${ESPPORT}""
#    echo TEST=$DOCKER_ESPPORT
#fi

docker run --rm -it --privileged \
  -e "ESPBAUD=921600" \
  $DOCKER_ESPPORT $DOCKER_USB_MAPPING_OPTION \
  -v $PWD:/home/esp/esp-open-rtos/examples/project \
  $DOCKER_IMG /bin/bash
