# https://stackoverflow.com/questions/40497214/what-should-be-linker-command-in-link-txt-of-cmake-when-cross-compiling#

# The Generic system name is used for embedded targets (targets without OS) in
# CMake
set(CMAKE_SYSTEM_NAME          Generic )
set(CMAKE_SYSTEM_PROCESSOR     ARM )

set(arch arm-none-eabi-)
set(triple 'arm-none-eabi')

# none of the examples explicitly state arch here EXCEPT for the working baremetal cortex github
set(CMAKE_C_COMPILER ${arch}gcc)
set(CMAKE_C_COMPILER_TARGET ${triple})

set(CMAKE_CXX_COMPILER ${arch}g++)
set(CMAKE_CXX_COMPILER_TARGET ${triple})

# tells CMake not to try to link executables during its interal checks
# things are not going to link properly without a linker script
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

#set(CMAKE_C_COMPILER_ID GNU)
#set(CMAKE_CXX_COMPILER_ID GNU)
#set(CMAKE_C_COMPILER_FORCED TRUE)
#set(CMAKE_CXX_COMPILER_FORCED TRUE)

set(CMAKE_LINKER arm-none-eabi-ld)


# We must set the OBJCOPY setting into cache so that it's available to the
# whole project. Otherwise, this does not get set into the CACHE and therefore
# the build doesn't know what the OBJCOPY filepath is
set(CMAKE_OBJCOPY arm-none-eabi-objcopy
      CACHE FILEPATH "The toolchain objcopy command " FORCE )


#SET(CMAKE_C_OUTPUT_EXTENSION ".o")