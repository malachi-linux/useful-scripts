#!/bin/bash

# just to speed along frequent comparisons to the same files

cmp -lb $1 "${CMP_PRESET_ORIGINAL}" "${CMP_PRESET_NEW}"

result=$?

if [ $result != '0' ]; then
	echo ---
	echo DIFF: $CMP_PRESET_ORIGINAL does not match $CMP_PRESET_NEW
else
	echo SAME: $CMP_PRESET_ORIGINAL matches $CMP_PRESET_NEW
fi