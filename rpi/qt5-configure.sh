#/bin/bash
# inspired from https://wiki.qt.io/RaspberryPi2EGLFS

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

. $SCRIPT_DIR/../ui/ask.sh

if [ -z "$RPIHOME" ]
then
    echo RPIHOME env variable must be set
    exit 1
fi
#RPIHOME=~/Projects/ext/rpi
QTHOSTPREFIX=$RPIHOME/out/qt5
QTEXTPREFIX=$RPIHOME/out/qt5pi
QTPREFIX=/usr/local/qt5pi
# tools from here git clone https://github.com/raspberrypi/tools
# likely can use other similar ARM cross compiler, but we'll follow the guide to the T
if [ -z "$GCCPREFIX" ]
then
   echo "GCCPREFIX env variable must be set (e.g. /path/to/arm-linux-gnueabihf-)"
   exit 1
fi
if ask "Use RPi2 config?" Y; then
   QTDEVICE=linux-rasp-pi2-g++
else
   QTDEVICE=linux-rasp-pi-g++
fi
#GCCPREFIX=$RPIHOME/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bin/arm-linux-gnueabihf-
if [ -z "$QTSYSROOT" ] 
then
    QTSYSROOT=$RPIHOME/sysroot
fi
FONTOPTIONS="-qt-freetype -fontconfig"
QTOPTIONS=""
# -qt-xcb affects X11 compatibility, but presently I am doing "Embedded Qt" 
# so X11 compatibility is not a concern.  We get compiler errors otherwise
#./configure -release -opengl es2 -qt-xcb -skip wayland -device linux-rasp-pi-g++ -device-option CROSS_COMPILE=$GCCPREFIX -sysroot $QTSYSROOT -opensource -confirm-license -make libs -prefix $QTPREFIX -extprefix $QTEXTPREFIX -hostprefix $QTHOSTPREFIX -v
./configure -release $FONTOPTIONS $QTOPTIONS -opengl es2 -device $QTDEVICE -device-option CROSS_COMPILE=$GCCPREFIX -sysroot $QTSYSROOT -no-gcc-sysroot -opensource -confirm-license -make libs -prefix $QTPREFIX -extprefix $QTEXTPREFIX -hostprefix $QTHOSTPREFIX -v

