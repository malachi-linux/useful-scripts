#/bin/bash
# lifted from https://wiki.qt.io/RaspberryPi2EGLFS
IP=$1
if [ -z "$2" ] 
  then
    USER=pi
else
    USER=$2
fi
rsync -avz $USER@$IP:/lib sysroot
rsync -avz $USER@$IP:/usr/include sysroot/usr
rsync -avz $USER@$IP:/usr/lib sysroot/usr
rsync -avz $USER@$IP:/opt/vc sysroot/opt
