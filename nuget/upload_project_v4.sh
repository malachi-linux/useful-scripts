#!/bin/bash

pushd $1

ASSEMBLY_NAME=$1.$VERSION
ASSEMBLY_PATH=bin/Debug/$ASSEMBLY_NAME

dotnet pack --version-suffix=$VERSION_SUFFIX

$NUGET push $ASSEMBLY_PATH.nupkg --source $NUGET_SOURCE -k $NUGET_KEY 
$NUGET push $ASSEMBLY_PATH.symbols.nupkg --source $NUGET_SOURCE -k $NUGET_KEY 

popd