#!/bin/bash

# do stock standard developer build for open source qt5

./configure -developer-build -opensource -nomake examples -nomake tests -confirm-license $1 $2
