#!/bin/bash

#DOCKER_IP=172.17.0.1:
# TODO: make these only defaults if not specified already on command line

DOCKER_NAME=bind
TAG=9.9.5-20170626
ROOT_PASSWORD=SecretPassword
DOCKER_DETACHED=-d

docker run $DOCKER_DETACHED --name=${DOCKER_NAME} --dns=127.0.0.1 \
  --publish=${DOCKER_IP}53:53/tcp \
  --publish=${DOCKER_IP}53:53/udp \
  --publish=${DOCKER_IP}10000:10000 \
  --volume=/srv/docker/bind:/data \
  --env="ROOT_PASSWORD=${ROOT_PASSWORD}" \
  sameersbn/bind:${TAG}
