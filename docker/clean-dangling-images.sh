#!/bin/bash

# from https://gist.github.com/bastman/5b57ddb3c11942094f8d0a97d461b430
# (have not tested this yet)

docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
docker rmi $(docker images | grep "none" | awk '/ / { print $3 }')
