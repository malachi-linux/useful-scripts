#!/bin/bash

# lifted from https://docs.docker.com/engine/installation/linux/docker-ce/debian/#install-using-the-repository
# this is special version for x64 Jessie/Stretch

sudo apt-get install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common

# get GPG key
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

sudo apt-get update

# now, do either:

# sudo apt-get install docker-ce
# or
# sudo apt-get install docker-ce=<VERSION_STRING>
# getting possible versions from - 
# apt-cache madison docker-ce
