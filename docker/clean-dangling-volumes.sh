#!/bin/bash

# from https://gist.github.com/bastman/5b57ddb3c11942094f8d0a97d461b430
docker volume rm $(docker volume ls -qf dangling=true)
