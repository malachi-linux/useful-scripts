#!/bin/bash
# grabs IP address for specified or all interfaces

ip addr show $1 | grep "inet " | awk '{print $2}'