#!/usr/bin/env python3

import sys, getopt, os

def show_help():
    print('test.py -i <inputfile> -o <outputfile>')

def main(argv):
    pvkFile = ''
    spcFile = ''
    # output
    pfxFile = ''
    try:
        opts, args = getopt.getopt(argv,"hpvk:spc:pfx:po:") #,["ifile=","ofile="])
    except getopt.GetoptError:
        show_help()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            show_help()
            sys.exit()
        elif opt == '-pvk':
            pass
        elif opt == '-spc':
            pass
        elif opt == '-pfx':
            pass

    # TODO: optimize and look for 'any' elements
    if len(opts) == 0:
        show_help()
        sys.exit()

#      elif opt in ("-i", "--ifile"):
#         inputfile = arg
#      elif opt in ("-o", "--ofile"):
#         outputfile = arg
    print('Input file is "', pvkFile)
    print('Output file is "', pfxFile)

    # generate intermediate PEM file from PVK
    # http://blog.gentilkiwi.com/cryptographie/openssl-conversion-pvk-microsoft-privatekey-blob
    os.system('openssl rsa -inform pvk -in {} -outform pem -out temp.pem'.format(pvkFile))
    # https://support.cloudbolt.io/hc/en-us/articles/115002452786-HOWTO-Convert-a-CER-file-to-CRT
    os.system('openssl x509 -inform DER -in {} -out temp.crt'.format(spcFile))

if __name__ == "__main__":
    main(sys.argv[1:])