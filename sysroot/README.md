# Overview

Slowly move things out of `rpi` folder here since it's largely a linux cross compile
question, not an rpi question specifically

Document v0.1

## 1. sysroot-tar.sh

DEFUNCT.  Recreating an entire tar from scratch is unappealing vs zip's ability
to update an existing one.

## 2. sysroot-zip.sh

EXPERIMENTAL Working better than `sysroot-tar` but it is so huge perhaps rsync really is the
superioer bet

## 3. sysroot-sync.sh

Like the one in `rpi` folder but goes the opposite direction - initiate from machine whose sysroot
you want copied
