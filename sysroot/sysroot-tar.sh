#!/bin/bash

# 19DEC23 - Non starter, compressed tar files can't be updated
# Use sysroot-gzip instead

export FILENAME=sysroot.tar.gz
export TAR_OPTIONS2="vzfr"
#export TAR_OPTIONS2="cvzf"

tar $TAR_OPTIONS2 $FILENAME /lib
#tar $TAR_OPTIONS $FILENAME /usr/include
tar $TAR_OPTIONS2 $FILENAME /opt
#rsync -avz $USER@$IP:/usr/lib sysroot/usr
#rsync -avz $USER@$IP:/opt/vc sysroot/opt
