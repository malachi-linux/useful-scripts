#!/bin/bash

# Guidance from
# https://unix.stackexchange.com/questions/368210/how-to-rsync-multiple-source-folders

# Keeping SYNC_TARGET and SYNC_TARGET_FOLDER separate so that quick command
# line treatment of 'target' can comfortably default to ".temp/sysroot"

if [ ! -z "$1" ]; then
    TARGET=$1
elif [ -z "$SYSROOT_SYNC_TARGET" ]; then
    echo "Target is required"
    exit 1
else
    TARGET=$SYSROOT_SYNC_TARGET
fi

# Overrides existing BASH $USER
if [ ! -z "$2" ]; then
    USER=$2
fi
if [ -z "$SYSROOT_SYNC_TARGET_FOLDER" ]; then
    FOLDER=".temp/sysroot"
else
    FOLDER=$SYSROOT_SYNC_TARGET_FOLDER
fi
export DEST=$USER@$TARGET:$FOLDER
#export RSOPT="-az --info=progress2"
#export RSOPT="-azhhnv --delete-during"
export RSOPT="-az --info=progress2 --relative"
# NOTE: /opt/vc is just for RPI, sometimes broadcom files are there
export RSFOLDERS="/lib /usr/include /usr/lib /usr/share/cmake* /opt/vc"

# Misbehaves
#rsync $RSOPT \
#    --exclude='*' \
#    --include='/lib/*' \
#    --include='/usr/include/***' \
#    --include='/usr/lib/*' \
#    / $DEST

#     --include=/opt/*** \

rsync $RSOPT $SYSROOT_SYNC_SOURCE_FOLDERS $RSFOLDERS $DEST
