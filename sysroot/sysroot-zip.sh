#!/bin/bash

FILENAME=sysroot
ZIPOPTS="-r --symlinks"

#rsync -avz $USER@$IP:/lib sysroot
#rsync -avz $USER@$IP:/usr/include sysroot/usr
#rsync -avz $USER@$IP:/usr/lib sysroot/usr
#rsync -avz $USER@$IP:/opt/vc sysroot/opt
zip $ZIPOPTS $FILENAME /lib
zip $ZIPOPTS $FILENAME /usr/include
zip $ZIPOPTS $FILENAME /usr/lib
zip $ZIPOPTS $FILENAME /opt
