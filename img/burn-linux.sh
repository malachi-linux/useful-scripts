#!/bin/bash

# Usage is 'burn-linux.sh infile.img /dev/outdevice'
# Experimental and untested support for piping

#SCRIPT_DIR=$(dirname "$(readlink "$0")")
SCRIPT_DIR=$MB_USEFUL_SCRIPTS

. $SCRIPT_DIR/ui/ask.sh

if [ -z "$1" ] 
then
    echo "Must specify which disk device.  Try one of these:"
    lsblk
    exit 1
fi

DISK_DEV=$1

if [ -z "$2" ]; then
    if [ -t 1 ]; then
        INPUT=sdcard.img
        echo "No input in interactive mode, assuming input file=$INPUT"
    fi
else
    INPUT=$2
fi

if [ -z "$INPUT" ]; then
    INPUT_PARAM=
else
    INPUT_PARAM="if=$INPUT"
fi

if ask "Do you really wanna burn to /dev/$DISK_DEV?" Y; then
    echo "Burning $INPUT to /dev/$DISK_DEV"
else
    echo "Not doing anything"
    exit 1
fi

HAVE_PV=$(which pv)
BLOCKSIZE=1024k

if [[ $HAVE_PV == /* ]]; then
    pv $INPUT | dd of=/dev/$DISK_DEV bs=$BLOCKSIZE
else
    dd if=$INPUT of=/dev/$DISK_DEV bs=$BLOCKSIZE
fi

sync
echo 3 > /proc/sys/vm/drop_caches
