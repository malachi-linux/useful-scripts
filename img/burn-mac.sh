#SCRIPT_DIR=$(dirname "$(readlink "$0")")
SCRIPT_DIR=$MB_USEFUL_SCRIPTS

. $SCRIPT_DIR/ui/ask.sh

if [ -z "$1" ] 
then
    echo "Must specify which disk device.  Try one of these (omitting /dev/):"
    diskutil list
    exit 1
fi

if [ -z "$2" ]; then
    INPUT=sdcard.img
else
    INPUT=$2
fi

DISK_DEV=$1

if ask "Do you really wanna burn $INPUT to /dev/$DISK_DEV?" Y; then
    echo "Burning"
else
    echo "Not doing anything"
    exit 1
fi

HAVE_PV=$(which pv)

diskutil unmountDisk /dev/$DISK_DEV
if [[ $HAVE_PV == /* ]]; then
    pv $INPUT | sudo dd of=/dev/r$DISK_DEV bs=1m
else
    sudo dd if=$INPUT of=/dev/r$DISK_DEV bs=1m
fi
diskutil unmountDisk /dev/$DISK_DEV
