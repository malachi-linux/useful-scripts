#/bin/bash
# we run parted and print out partition table for $IMG
# first, we grep " 2 " to find 2nd partition (hardcoded)
# second, we awk & grab 2nd parameter from parted list (1st is part#, 2nd is offset in bytes)
# third, we sed to strip off trailing B (bytes)

IMG=$1
OFFSET=$(parted $IMG unit b print \
	| grep " 2 " \
	| awk '{print $2;}' \
	| sed -e 's/B//')

echo Offset = $OFFSET
mount -o loop,offset=$OFFSET $IMG /mnt/rasp-pi-rootfs

