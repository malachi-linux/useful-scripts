#!/bin/bash

# bumps a version # up for this branch
# TODO: add a provision for reset

US=$MB_USEFUL_SCRIPTS
branch_name=$(${US}/scm/get-branch-name.sh)

# if no branch_folder already specified
if [ -z $branch_folder ]; then
	# if no parameter is specified to explicitly state branch_folder
	if [ -z $1 ]; then
		# default to current folder
		branch_folder=.
	else
		# otherwise point to explicit location
		branch_folder=$1
	fi
fi

# place where actual version metafiles live
metafolder=${branch_folder}/.version

old_filename=${branch_folder}/.version.$branch_name
new_filename=${metafolder}/${branch_name}

if [ ! -d "$metafolder" ]; then
	mkdir $metafolder
	echo 0 > $new_filename
fi

# cheezy code to help transition 'collection' repo
if [ ! -z $version_newmode ]; then
	filename=$new_filename
else
	filename=$old_filename
fi

branch_version=$(<$filename)

if [ -z "$branch_version" ]
then
	echo "Unable to bump branch version.  Ensure file '$filename' exists"
else
	((branch_version++))

	echo $branch_version > $filename
	echo $branch_version > $new_filename

	git commit -am "Version bump to $branch_version on branch: $branch_name"
fi
