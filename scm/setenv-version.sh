#!/bin/bash

# v1.0 helper script for setting up semver 2.0 spec

version_path=$1
if [ -z $2 ]
then
	branch_name=$(${MB_USEFUL_SCRIPTS}/scm/get-branch-name.sh)
else
	branch_name=$2
fi

branch_version=$(<$version_path/.version.$branch_name)

project_version=$(<$version_path/.version/prefix.$branch_name)

if [ -z "$branch_version" ]
then
	branch_version=$(<$version_path/.version/$branch_name)

	if [ -z "$branch_version" ]
	then
		echo "No version available for this branch.  Do not upload NUGET packages"
		branch_version="0"
	fi
fi

if [ -z $project_version ]
then
	project_version=$(<$version_path/.project-version)
fi

if [ -z $project_version ]
then
	echo "No version available for this project.  Do not upload NUGET packages"
	project_version="0.0.1"
fi

# https://semver.org/#spec-item-9 indicates NO leading zeros like the old days
printf -v branch_version ".%d" $branch_version

export VERSION_PREFIX=$project_version
export VERSION_SUFFIX=$branch_name$branch_version
export VERSION=$VERSION_PREFIX-$VERSION_SUFFIX

echo "Using VERSION=$VERSION"
